<? include("includes/include.inc.php");
$faq_sql=mysql_query("select * from gws_manage_faq where status='Active' ORDER BY id ASC");
$faq_num=mysql_num_rows($faq_sql);

?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>FAQs</title>
<meta name="keywords" content="">
<meta name="description" content="">
 <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<!-- Custom css -->
	 <link href="css/main.css" rel="stylesheet">
	  <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
      <link href="faq/css/responsive-accordion.css" rel="stylesheet">
	 
	 <link rel="stylesheet" href="<?=SITE_WS_PATH?>stylesheets/babylongrid-default.css">
	 <link rel="icon" href="/images/favicon.png" type="image/png" sizes="16x16">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href="faq/css/responsive-accordion.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>

<!--Header End -->
<? include('header.php'); ?>
<!--Header End -->
<!--Breadcrumbs start-->
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 box-padding">
<ol class="breadcrumb">
<li><a href="index.php">Home</a></li>
<li class="active"><a href="#">Faqs</a></li>
</ol>

<div class="border">
<h2>FAQS</h2>

<div class="concern">
<? if($faq_num>0){?>  
		  
			<ul class="responsive-accordion responsive-accordion-default bm-larger">
			<? while($faq_row=mysql_fetch_array($faq_sql)){?>
				<li>
				<div class="responsive-accordion-head"><?=$faq_row[question]?><i class="fa fa-chevron-down responsive-accordion-plus fa-fw" 			 				style="display: block;"></i>
				<i class="fa fa-chevron-up responsive-accordion-minus fa-fw" style="display: none;"></i></div>
				<div class="responsive-accordion-panel" style="display: none;">
				<p><?=$faq_row[answer]?></p>	
				</div>
				</li>
				<? }?>
					
</ul>
<? } else {?>
<p>No Record Found. We are updating content of this page. Kindly revisit this page soon.</p>
<? }?>

			
		</div>









    </div>
    <!--container--> 
  </div>
  <!--grey-color--> 
</div>
</div>
</div>
</div>
<!-- CONTACT FORM END
========================================================================= --> 

<!-- FOOTER STARTS
========================================================================= -->
<? include('footer.php'); ?>

  <!-- FOOTER END
========================================================================= -->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=SITE_WS_PATH?>js/bootstrap.js"></script> 
    <script src="faq/js/smoothscroll.min.js" type="text/javascript"></script>
		<script src="faq/js/backbone.js" type="text/javascript"></script>
		<script src="faq/js/responsive-accordion.min.js" type="text/javascript"></script>
</body>
</html>
