<?
include("../includes/include.inc.php");
//date_default_timezone_set('Asia/Calcutta');
$date = date("Y-m-d H:i:s");

$get_data = "select * from gws_post_jobs order by id";
$res_data = mysql_query($get_data) or die(mysql_error());

if ($_POST['Submit']=='update'){
    $job_status = $_POST['job_status'];
    $id = $_POST['id'];
    mysql_query("UPDATE `gws_post_jobs` SET `status`='$job_status'
 WHERE `id` = '$id'")or die(mysql_error());

    $_SESSION['SESS_MSG']="Record Updated succesfully!";
    header("location:all-job-posts.php");
    exit;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>

    <!------------------Here Using Price Button Ajax Code Here    -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <!-- start: Meta -->
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="Bootstrap Metro Dashboard">
    <meta name="author" content="Dennis Ji">
    <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <!-- end: Meta -->

    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->

    <!-- start: CSS -->
    <link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link id="base-style" href="css/style.css" rel="stylesheet">
    <link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
    <!-- end: CSS -->


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link id="ie-style" href="css/ie.css" rel="stylesheet">
    <![endif]-->

    <!--[if IE 9]>
    <link id="ie9style" href="css/ie9.css" rel="stylesheet">
    <![endif]-->

    <!-- start: Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- end: Favicon -->
</head>

<body>
<!-- start: Header -->
<!-- start: Header Menu -->
<? include('header.php');?>
<!-- end: Header Menu -->
<!-- start: Header 3  div here  -->
<!--start: page content area-->
<div class="container-fluid-full">
    <!-- start: left Menu -->
    <div class="row-fluid">
        <?  include('leftnav.php');  ?>
        <!-- end: left Menu -->
        <!-- start: Content -->
        <div id="content" class="span10">
            <ul class="breadcrumb">
                <li> <i class="icon-home"></i> <a href="index.html">Home</a> <i class="icon-angle-right"></i> </li>
                <li><a href="#">Manage Job Posts</a></li>
            </ul>
            <div class="row-fluid sortable">
                <? include("../includes/front.msg.inc.php");?>
                <div class="box span12">
                    <div class="box-header" data-original-title>
                        <h2><i class="halflings-icon edit"></i><span class="break"></span>
                            <? if($_GET['id']==''){?>
                                All Jobs
                            <? } else {?>
                                Edit Job
                            <? }?>
                        </h2>
                        <div class="box-icon"> <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a> <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a></div>

                    </div>
                    <? if($_GET['id']!='') {
                        $edit_id = $_GET['id'];
                        $get_edit_data = "select * from gws_post_jobs where id='$edit_id' ";
                        $res_edit_data = mysql_query($get_edit_data) or die(mysql_error());
                        $get_res_edit_data=mysql_fetch_array($res_edit_data);
                        $cat_id = $get_res_edit_data['cat_id'];
                        $member_id = $get_res_edit_data['mem_id'];
                        $get_cat_data = "select * from gws_post_job_categories where id = '$cat_id'";
                        $res_cat_data = mysql_query($get_cat_data) or die(mysql_error());
                        $get_res_cat_data=mysql_fetch_array($res_cat_data);
                        $get_mem_data = "select * from gws_members where id = '$member_id'";
                        $res_mem_data = mysql_query($get_mem_data) or die(mysql_error());
                        $get_res_mem_data=mysql_fetch_array($res_mem_data);
                        ?>

                        <div class="box-content">
                            <form id="form1" name="form1" method="post" action="all-job-posts.php" enctype="multipart/form-data">
                                <fieldset>

                                    <!-- success Alert for sub category page -->
                                    <input type="hidden" id="id" name="id" value="<?php echo $edit_id;?>"/>
                                    <div class="control-group">
                                        <label class="control-label" for="job_category">Job Category</label>
                                        <div class="controls">
                                            <input type="text" value="<?php echo $get_res_cat_data['cat_name']?>" disabled class="input-xlarge focused" id="job_category"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="job_title">Job Title</label>
                                        <div class="controls">
                                            <input type="text" value="<?php echo $get_res_edit_data['job_title']?>" disabled class="input-xlarge focused" id="job_title"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="job_desc">Job Description</label>
                                        <div class="controls">
                                            <textarea disabled class="input-xlarge focused" rows="6"><?php echo $get_res_edit_data['job_desc']?></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="job_city">Job City</label>
                                        <div class="controls">
                                            <input type="text" value="<?php echo $get_res_edit_data['job_city']?>" disabled class="input-xlarge focused" id="job_city"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="job_state">Job State</label>
                                        <div class="controls">
                                            <input type="text" value="<?php echo $get_res_edit_data['job_state']?>" disabled class="input-xlarge focused" id="job_state"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="job_zip">Job Zip Code</label>
                                        <div class="controls">
                                            <input type="text" value="<?php echo $get_res_edit_data['job_zip']?>" disabled class="input-xlarge focused" id="job_zip"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="additional_info">Additional Info</label>
                                        <div class="controls">
                                            <textarea disabled class="input-xlarge focused" rows="6"><?php echo $get_res_edit_data['additional_info']?></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="job_status">Status</label>
                                        <select class="input-xlarge focused" name="job_status" id="job_status">
                                            <option value="1" <?php if ($get_res_edit_data['status']==1){?>selected<?php }?>>Pending</option>
                                            <option value="2" <?php if ($get_res_edit_data['status']==2){?>selected<?php }?>>Approved</option>
                                            <option value="3" <?php if ($get_res_edit_data['status']==3){?>selected<?php }?>>Cancel</option>
                                        </select>
                                    </div>

                                    <!--select a category-->
                                    <div class="form-actions">
                                        <button name="Submit" type="submit" class="btn btn-primary" value="update">Save</button>
                                        <input type="reset" class="btn" value="Cancel">
                                        <!-- <button type="reset"Cancel</button>-->
                                        <!-- add if new entry or update if existing-->
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <!--/span-->

                <div class="row-fluid sortable">
                    <div class="box span12">
                        <!-- table header -->
                        <div class="box-header">
                            <h2><i class="halflings-icon align-justify"></i><span class="break"></span>Manage Job Posts</h2>
                            <div class="box-icon"> <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a> <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a> </div>
                        </div>
                        <!-- table header ends -->
                        <div class="box-content">
                            <form name="form2" method="post" action="" >
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th width="3%">SN</th>
                                        <th>Category</th>
                                        <th>Member Name</th>
                                        <th>Job Title</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <!--record strt search-->
                                    <!--record strt search-->
                                    <tbody>
                                    <? if($res_data==0){ ?>
                                        <tr>
                                            <td colspan="12" class="center"><strong>Sorry, no record found.</strong></td>
                                        </tr>
                                    <? }else{ ?>
                                        <?

                                        $i=$start+1;
                                        while($line=mysql_fetch_array($res_data)){
                                            $cat_id = $line['cat_id'];
                                            $member_id = $line['mem_id'];
                                            $get_cat_data = "select * from gws_post_job_categories where id = '$cat_id'";
                                            $res_cat_data = mysql_query($get_cat_data) or die(mysql_error());
                                            $get_res_cat_data=mysql_fetch_array($res_cat_data);
                                            $get_mem_data = "select * from gws_members where id = '$member_id'";
                                            $res_mem_data = mysql_query($get_mem_data) or die(mysql_error());
                                            $get_res_mem_data=mysql_fetch_array($res_mem_data);

                                            if($i%2==0){
                                                $bg='#e6e6e6';
                                            }else{
                                                $bg='#FFFFFF';
                                            }
                                            ?>
                                            <tr>
                                                <td><?=$i ?></td>
                                                <td><?=$get_res_cat_data['cat_name']?></td>
                                                <td><?=$get_res_mem_data['email']?></td>
                                                <td><?=$line['job_title']?></td>
                                                <td><?=$line['added_date']?></td>
                                                <td><? if($line['status']=='1'){?>
                                                        <span class="label label-warning">Pending</span>
                                                    <? }
                                                    elseif ($line['status']=='2'){
                                                    ?>
                                                        <span class="label label-success">Approve</span>
                                                        <?php
                                                    }
                                                    else {?>
                                                        <span class="label label-danger">Cancel</span>
                                                    <? }?>
                                                </td>
                                                <td>
                                                    <a class="btn btn-info" href="all-job-posts.php?id=<?php echo $line['id']?>"><i class="halflings-icon white edit"></i></a>
                                                    <a class="btn btn-danger" href="all-job-posts.php?type=delete&id=<?php echo $line['id']?>" ><i class="halflings-icon white trash"></i></a>
                                                </td>
                                            </tr>
                                            <? $i++; }} ?>
                                    </tbody>
                                </table>

                            </form>
                        </div>
                    </div>
                    <!--/span-->
                </div>

            </div>

            <!--/row-->
        </div><!--/.fluid-container-->
        <!-- end: Content -->
    </div><!--/#content.span10-->
</div><!--/fluid-row-->
<div class="modal hide fade" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">?</button>
        <h3>Settings</h3>
    </div>
    <div class="modal-body">
        <p>Here settings can be configured...</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
    </div>
</div>
<div class="clearfix"></div>

<footer>

    <? include('footer.php');?>

</footer>



<!-- start: JavaScript-->

<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-migrate-1.0.0.min.js"></script>

<script src="js/jquery-ui-1.10.0.custom.min.js"></script>

<script src="js/jquery.ui.touch-punch.js"></script>

<script src="js/modernizr.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/jquery.cookie.js"></script>

<script src='js/fullcalendar.min.js'></script>

<script src='js/jquery.dataTables.min.js'></script>

<script src="js/excanvas.js"></script>
<script src="js/jquery.flot.js"></script>
<script src="js/jquery.flot.pie.js"></script>
<script src="js/jquery.flot.stack.js"></script>
<script src="js/jquery.flot.resize.min.js"></script>

<script src="js/jquery.chosen.min.js"></script>

<script src="js/jquery.uniform.min.js"></script>

<script src="js/jquery.cleditor.min.js"></script>

<script src="js/jquery.noty.js"></script>

<script src="js/jquery.elfinder.min.js"></script>

<script src="js/jquery.raty.min.js"></script>

<script src="js/jquery.iphone.toggle.js"></script>

<script src="js/jquery.uploadify-3.1.min.js"></script>

<script src="js/jquery.gritter.min.js"></script>

<script src="js/jquery.imagesloaded.js"></script>

<script src="js/jquery.masonry.min.js"></script>

<script src="js/jquery.knob.modified.js"></script>

<script src="js/jquery.sparkline.min.js"></script>

<script src="js/counter.js"></script>

<script src="js/retina.js"></script>

<script src="js/custom.js"></script>
<!-- end: JavaScript-->

</body>
</html>





>>