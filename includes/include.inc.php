<?

error_reporting(E_ALL ^ E_NOTICE);
@session_start();

// Start output buffering
// Set variable $NO_OB = true if output buffering is not needed in a file
/*
if(!isset($NO_OB) && LOCAL_MODE==false) {
	ob_start("ob_gzhandler");
}
*/
if ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "127.0.0.1") 
{
    define('LOCAL_MODE', true);
}else{
 	define('LOCAL_MODE', false);
}
// File system path

$tmp = dirname(__FILE__);
$tmp = str_replace('\\' ,'/',$tmp);
$tmp = substr($tmp, 0, strrpos($tmp, '/'));
define('SITE_FS_PATH', $tmp); 

require_once(SITE_FS_PATH."/includes/config.php");
require_once(SITE_FS_PATH."/includes/function.php");
require_once(SITE_FS_PATH."/includes/array.inc.php");
require_once(SITE_FS_PATH."/includes/class.phpmailer.php");

date_default_timezone_set('EST');

// Script start time used to test site performance
define('SCRIPT_START_TIME', getmicrotime());
//echo("<br>SCRIPT_START_TIME: ".SCRIPT_START_TIME);

// magic_quotes_gpc needs to be "on"
if(!get_magic_quotes_gpc()) {
	$_GET		= ms_addslashes($_GET);
	$_POST		= ms_addslashes($_POST);
	$_COOKIE	= ms_addslashes($_COOKIE);
} else {
	$_GET		= ms_trim($_GET);
	$_POST		= ms_trim($_POST);
	$_COOKIE	= ms_trim($_COOKIE);// trim is used to remove white space
}

//import_request_variables('gp');

// magic_quotes_runtime needs to be "off"
if(get_magic_quotes_runtime()) {
	set_magic_quotes_runtime(0);
}

// Protect admin pages
$PHP_SELF = $_SERVER['PHP_SELF'];
$admin_pos  = strpos($PHP_SELF, '/admin/');
if($admin_pos !== false) {
	// Remove following comment to enable admin login check
	//protect_admin_page();
}
// Set variable $NO_CON to anything if DB connection is not needed in file
if(!isset($NO_CON)) { 
	connect_db(); // Opening DB connection
}

require_once(SITE_FS_PATH."/includes/config-user.php");

@extract($_POST);
@extract($_GET);

ob_start();

$search  = array('-', '_', 'and');
$replace = array(' ', '/', '&');


?>