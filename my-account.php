<? include("includes/include.inc.php");
protect_member_page();
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?=$setting_data['meta_tag']?></title>
<meta name="keywords" content="">
<meta name="description" content="">
 <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<!-- Custom css -->
	 <link href="css/main.css" rel="stylesheet">
	  <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
	 
	 <link rel="stylesheet" href="stylesheets/babylongrid-default.css">
	<link rel="icon" href="/images/favicon.png" type="image/png" sizes="16x16">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
<body>

<!--Header End -->
<? include('header2.php'); ?>
<!--Header End -->
<!--Breadcrumbs start-->
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12">
<ol class="breadcrumb">
  <li><a href="index.php">Home</a></li>
  <li class="active"><a href="#">About Us</a></li>
  
  <div class="panel panel-default">
	 
	<div class="panel-heading">Terms of Use</div>
	 
	<div class="panel-body">  
		
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sed massa tincidunt, aliquet enim interdum, luctus purus. Donec ut urna non diam auctor euismod. Fusce lobortis vitae massa facilisis fringilla. Morbi sed nulla sit amet lacus hendrerit egestas. Proin facilisis diam ac ultrices condimentum. Praesent at molestie tellus, eget eleifend neque. Nunc in varius est. Nulla imperdiet, diam quis efficitur luctus, erat mauris semper est, sed sodales lectus velit et ex. Donec vitae diam quis mauris pharetra porttitor. Curabitur mauris arcu, auctor id fermentum a, pretium quis mauris.

Vestibulum velit felis, lobortis sed risus at, feugiat pretium nisl. In facilisis turpis quis laoreet viverra. Proin tempor accumsan bibendum. Phasellus odio tortor, sollicitudin non auctor eu, consequat in ipsum. Fusce tristique luctus pellentesque. Etiam porttitor sodales ipsum. Suspendisse ut placerat erat, sed mattis elit. Ut semper mi tellus, interdum ornare nisl aliquam non. Vestibulum elementum venenatis lorem, nec faucibus libero porta a. Morbi tincidunt sollicitudin odio, nec gravida ex egestas a. Maecenas et efficitur purus. Cras sit amet augue sem. Mauris posuere, leo et aliquet facilisis, odio massa condimentum felis, et luctus felis lorem eget lorem. Sed vehicula turpis magna, in tempus erat imperdiet sit amet. Vestibulum eget suscipit diam. Phasellus eu orci ultrices, egestas leo dapibus, porta lorem.

Cras mi magna, tincidunt in augue nec, semper varius metus. Nunc porta risus risus, sit amet rhoncus eros facilisis id. Integer id fermentum nulla, ac viverra leo. Vestibulum ullamcorper nulla enim, vel consequat mi vestibulum in. Vestibulum hendrerit magna vel hendrerit volutpat. Fusce feugiat odio turpis, sit amet convallis quam rhoncus vel. Proin ligula ipsum, malesuada sit amet bibendum ac, faucibus a risus. Aliquam condimentum diam id purus dignissim, at tristique massa fermentum. Nam interdum efficitur nisi, in finibus leo aliquet a. Nunc viverra et nisi sit amet varius. In tincidunt odio erat, sed tempus leo condimentum vel. Vestibulum mauris erat, aliquam non enim id, congue pellentesque mi. Cras sed malesuada neque, a varius lorem. Praesent volutpat ligula a lectus pharetra consequat a vitae est</p>

 
	</div>
	 
	</div>
  
  
</ol>
</div>
</div>
</div>
<!--Breadcrumbs end-->
<!-- Registration FORM
========================================================================= -->
<!--<div class="col-xs-12 box-padding">
	<div class="border">
  <div class="container box-padding text-center" id="registration-form">
  <h2 class="black">Log In</h2>
    <p> Already Registered with YipYak? Click here to <a href="login.php">Log In</a></p> 	   
    <div class="row">
      <article class="col-md-12 col-xs-12 textbox text-center">
        <div class="col-sm-9 col-xs-12 registration-form">
		      
		    <form  action="" name="" method="post">
			
			
			<div class="col-sm-12 col-xs-12">
                <input type="email" name="member_email" id="subject2" class="form-control registration-form-control" placeholder="Enter Your Email Id:" required="true" /> <span class="gray-color">*</span>
            </div>
              
		      <div class="col-sm-12 col-xs-12"><input  type="password" name="password" id="password" class="form-control registration-form-control" placeholder="Enter Your Password:" required="true" /><span class="gray-color">*</span> </div>
			  
			  <div class="clear"></div>
              
              
                  
			        
        
			          
				  <div class="col-sm-6 col-xs-12">
				  <div class="checkbox">
  					 <label>
     				 <input type="checkbox"> I agree to the website <a href="/terms-of-use" text-decoration:underline;" 
                     			target="_blank">Terms of Use</a> & <a href="/Privacy-policy"  text-decoration:underline;" 
                                target="_blank">Privacy Policy</a>
   					</label>
 					 </div>	
                     </div>	
		</form>

					<div class="col-sm-12 col-xs-12">
                    	<div class="submit">
                                <button type="submit" id="submit" class="btn btn-primary btn-md" aria-expanded="false" name="submit" >Submit Now</button>
                                
                                     </div>
                                     </div><!--submit--->
			
          
          
          <!--inquiry-form"--> 
        </div>
      </article>
    </div>
    <!--container--> 
  </div>
  <!--grey-color--> 
</div>
</div>
</div>
</div>
<!-- CONTACT FORM END
========================================================================= --> 

<!-- FOOTER STARTS
========================================================================= -->
<? include('footer.php'); ?>

  <!-- FOOTER END
========================================================================= -->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=SITE_WS_PATH?>js/bootstrap.js"></script> 
</body>
</html>
