<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title></title>
<meta name="keywords" content="">
<meta name="description" content="">
 <!-- Bootstrap -->
    <link href="../css/bootstrap.css" rel="stylesheet">
	<!-- Custom css -->
	 <link href="../css/main.css" rel="stylesheet">
	  <link href="<?=SITE_WS_PATH?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
	 
	 <link rel="stylesheet" href="<?=SITE_WS_PATH?>stylesheets/babylongrid-default.css">
	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

</head>
<body>
<? include('../header.php'); ?>
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12">
<ol class="breadcrumb">
  <li><a href="../index.php">Home</a></li>
  <? if($_SESSION['RENEW']!='Yes'){?>
  <li class="active"><a href="#">Ad Posting Payment Process</a></li>
  <? } else { ?>
   <li class="active"><a href="#">Ad Renewal Payment Process</a></li>
  <? } ?>
</ol>
</div>
</div>

<div class="row">
<div class="col-md-12 col-sm-12">
<? $member_detail=mysql_fetch_array(mysql_query("select * from gws_members where id='".$_SESSION['MEMBER_ID']."'"));?>
<p>Dear <?=$member_detail[screen_name]?>,</p>


</div>
</div>


<?php
//date_default_timezone_set('UTC');
$date = date("Y-m-d H:i:s");

require_once("class_gateway.php");
include("../includes/include.inc.php");
	if ($_POST["check"] != '')
	{
		$request["DCI"] 							= "0";
		$request["INSTALLMENT_SEQUENCENUM"] 		= "0";
		$request["OVERRIDE_FROM"] 					= "0";
		$request["RETAIL_LANENUM"] 					= "0";
		$request["TOTAL_INSTALLMENTCOUNT"] 			= "0";
		$request["TRANSACTION_SERVICE"] 			= "0";
		$request["CARDHOLDER_FIRSTNAME"] 			= $_POST["cardholder_firstName"];
		$request["CARDHOLDER_LASTNAME"] 			= $_POST["cardholder_lastName"];
		$request["AMOUNT"] 							= $_POST["transamount"];
		$request["CARD"]["CARDCODE"] 				= $_POST["cvvc"];
		$request["CARD"]["CARDNUMBER"] 				= $_POST["creditcardnum"];
		$request["CARD"]["EXPDATE"] 				= $_POST["ExpMonth"] ."".$_POST["ExpYear"];
		$request["CODE"] 							= $_POST["transactiontype"];
		$request["MERCHANT_KEY"]["GROUPID"] 		= "0";
		$request["MERCHANT_KEY"]["SECUREKEY"] 		= "9gYlU8B49nhW";
		$request["MERCHANT_KEY"]["SECURENETID"] 	= "8005274";
		$request["METHOD"] 							= "CC";
		$request["ORDERID"] 						= $_POST["orderid"];
		$request["INDUSTRYSPECIFICDATA"]=$_POST["INDUSTRYSPECIFICDATA"];
		$transactionType = $_POST["transactiontype"];
		if ($transactionType == "0200" || $transactionType == "0400" || $transactionType == "0300" || $transactionType == "0600")
		{
			$request["AUTHCODE"]						= $_POST["authroizationcode"];
		}
		else
		{
			$request["AUTHCODE"]						= "";
		}
		if ($transactionType == "0200" || $transactionType == "0400" || $transactionType == "0500")
		{	
			$request["REF_TRANSID"] 					= $_POST["transactionid"];
		}
		else
		{
			$request["REF_TRANSID"] 					= "";
		}
		if($_POST["istest"] == "1")
		{
			$request["TEST"] 							= $_POST["TRUE"];
		}
		else
		{
			$request["TEST"] 							= $_POST["FALSE"];
		}		
		try
		{
			$gw = new Gateway(3);
			$response = $gw->ProcessTransaction($request);
		}
		catch(Exception $e)
		{
			echo "Exception: ".$e->getMessage()."\n";
		}
		echo "<br><strong>Transaction Response</strong><br><br>Response Reason Code = " . $response->ProcessTransactionResult->TRANSACTIONRESPONSE->RESPONSE_CODE;
		echo "<br>Response Reason Text = " . $response->ProcessTransactionResult->TRANSACTIONRESPONSE->RESPONSE_REASON_TEXT;
		if($response->ProcessTransactionResult->TRANSACTIONRESPONSE->RESPONSE_CODE == "1")
		{
			echo "<br>Authorization Code = " . $response->ProcessTransactionResult->TRANSACTIONRESPONSE->AUTHCODE;
			echo "<br>Transaction Id = " . $response->ProcessTransactionResult->TRANSACTIONRESPONSE->TRANSACTIONID;
			$transaction_id=$response->ProcessTransactionResult->TRANSACTIONRESPONSE->TRANSACTIONID;
			
			
			if($_SESSION['RENEW']!='Yes'){
			echo "You have successfully posted your Ad. The Transactions details are given below for your quick review. You may edit your ad by visiting your My Account Section at any time.";
			}else {
			echo "You have successfully Renewed your Ad for 30 days";
			}
			
			
			
			if($_SESSION['RENEW']!='Yes'){
			
			$dat_set=mysql_fetch_array(mysql_query("select * from gws_ad where order_id='".$_SESSION['order_id']."'"));			
			$time_stamp = date('Y-m-d', strtotime($date. ' + '. $dat_set[ad_duration].' days'));
			$time_stamp1 = strtotime($time_stamp);
			$expiry_date = date('Y-m-d', $time_stamp1);								
			
			$sql=mysql_query("update gws_ad set payment_status='paid',
			payment_date='$date',
			ad_active_date='$date',
			transaction_id='$transaction_id',
			ad_price='".$_SESSION['AD_PRICE']."',
			preview='No',			
			expiry_date='$expiry_date',
			status='Active' where order_id='".$_SESSION['order_id']."'
			");
			
			}
			else {	
$renew_ad_details=mysql_fetch_array(mysql_query("select * from gws_ad where id='".$_SESSION['RENEW_ID']."'"));	

$package_day=mysql_fetch_array(mysql_query("select * from gws_packages where id='$renew_ad_details[package_id]'"));	
		
$insert_renew=mysql_query("insert into gws_ad_renew set ad_id='".$_SESSION['RENEW_ID']."',
														  order_id='".$_SESSION['RENEW_ORDER_ID']."',
														  transation_id='$transaction_id',
														  price='".$_SESSION['RENEW_PRICE']."',
														  renewal_date='$date',
														  payment_status='paid',
														  payment_date='$date', status='Active'");

//ad_active_date and ad_duration calculate
$dat1 = $renew_ad_details[expiry_date];
$dat2 = strtotime($dat1);
$dat3 = time($date);
$subTime1 = $dat2 - $dat3;
$d = ($subTime1/(60*60*24))%365;
//$d1=$renew_ad_details[ad_duration];
//$no_days=$d1-$d;    //ad_duration
//end ad_active_date and ad_duration calculate

//expiry date calculate
//$total_days=$no_days+$package_day[duration];

$total_days=$d+$package_day[duration];
//echo $total_days;
$timestamp_d = date('Y-m-d', strtotime($date. ' + '. $total_days.' days'));	
$timestamp_m = strtotime($timestamp_d);
$new_expiry_date = date('Y-m-d', $timestamp_m);							
$expiry_date=$new_expiry_date;
//end expiry date calculation


$update_order=mysql_query("update gws_ad set renew='Yes',											 
											 renewal_date='$date',											 
											 ad_active_date='$date',
											 expiry_date='$expiry_date',
											 mod_date='$date',
											 status='Active'
											 where id='".$_SESSION['RENEW_ID']."'");

//$order_id=$_SESSION['order_id']+1;
//echo $order_id;
//echo $_SESSION['RENEW_ORDER_ID'];
//mysql_query("update gws_settings set order_id='".$_SESSION['RENEW_ORDER_ID']."'");

			
		//	$renew_ad_details=mysql_fetch_array(mysql_query("select * from gws_ad where id='".$_GET['renew_ad']."'"));
//			$setting_data=mysql_fetch_array(mysql_query("select * from gws_settings where 1"));
//			$renew_amount=$setting_data['renew_amount'];
//			$insert_renew=mysql_query("insert into gws_ad_renew set ad_id='".$_GET['renew_ad']."', renewal_date='$today'");
//			$update_order=mysql_query("update gws_ad set renew='Yes', renewal_date='$today',
//														 ad_active_date='$today',
//														 expiry_date='$expiry_date' 
//														 where id='".$_GET['renew_ad']."''");				 

			}
			
		}	
	//	echo "<br><br><a href='gatewayapi.php'>Try again</a>";
	}
	else
	{	
	echo "There was an error with the payment, please try again!";
		@header("Location: payment.php");
	}
?>
</div>
<? include('../footer.php'); ?>

  <!-- FOOTER END
========================================================================= -->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=SITE_WS_PATH?>js/bootstrap.js"></script> 
</body>
</html>
<? unset($_SESSION['RENEW_ORDER_ID']); ?>
<? unset($_SESSION['RENEW_ID']); ?>
<? unset($_SESSION['RENEW_PRICE']); ?>
<? unset($_SESSION['AD_PRICE']); ?>