<? include("includes/include.inc.php");
//protect_member_page();
//date_default_timezone_set('Asia/Calcutta');
$today = date("Y-m-d H:i:s");
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title></title>
<meta name="keywords" content="">
<meta name="description" content="">
 <!-- Bootstrap -->
    <link href="<?=SITE_WS_PATH?>css/bootstrap.css" rel="stylesheet">
	<!-- Custom css -->
	 <link href="<?=SITE_WS_PATH?>css/main.css" rel="stylesheet">
	  <link href="<?=SITE_WS_PATH?>fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
	 
	 <link rel="stylesheet" href="<?=SITE_WS_PATH?>stylesheets/babylongrid-default.css">
	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
<body>

<!--Header End -->
<? include('header.php'); ?>
<!--Header End -->
<!--Breadcrumbs start-->
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12">
<ol class="breadcrumb">
<li><a href="<?=SITE_WS_PATH?>index.php">Home</a></li>
<!--<li class="active"><a href="myaccount.php">My Account</a></li>-->
<li>Access Denied</li>
  
</ol>
</div>
</div>
<!---------------------------------Breadcrumbs end--------------------------------------->  

    
<article class="col-md-9 col-sm-9 box-padding">
     <div class="taxonomytitle">
     <h1>Access Denied</h1>
     </div>
     <div class="clearfix"></div>
     <hr>

	 <i class="fa fa-frown-o" style="font-size:60px; float:left; margin-right:20px; "></i>
    <h4>Sorry, You can not access this page... </h4>
    <div class="clearfix"></div>
    <hr>
  
            
</article>   
  			
      
<div class="col-md-12 col-sm-12"><div class=" border"></div></div>
</div><!---container---->


<!-- FOOTER STARTS
========================================================================= -->
<? include('footer.php'); ?>

  <!-- FOOTER END
========================================================================= -->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=SITE_WS_PATH?>js/bootstrap.js"></script> 
</body>
</html>
