<? include("includes/include.inc.php");
protect_member_page();
//date_default_timezone_set('Asia/Calcutta');
$today = date("Y-m-d H:i:s");
$member_data=mysql_fetch_array(mysql_query("select * from gws_members where id='".$_SESSION['MEMBER_ID']."'")); // from member table
$member_login=mysql_query("select * from gws_loginlogs where member_id='".$_SESSION['MEMBER_ID']."'");
$num_rows=mysql_num_rows($member_login);


$mem=mysql_fetch_array(mysql_query("select * from gws_members where id='".$_SESSION['MEMBER_ID']."'"));
$date1 = $mem[add_date];

$date = strtotime($date1);
$date2 = time($today);

$subTime = $date2 - $date;
$y = ($subTime/(60*60*24*365));
$mn = ($subTime/(60*60*24*30));
$d = ($subTime/(60*60*24))%365;
$h = ($subTime/(60*60))%24;
$m = ($subTime/60)%60;
$day=$d-(floor($mn)*30);

?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>My Account</title>
<meta name="keywords" content="">
<meta name="description" content="">
 <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<!-- Custom css -->
	 <link href="css/main.css" rel="stylesheet">
	  <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
	 <link rel="icon" href="/images/favicon.png" type="image/png" sizes="16x16">
	 <link rel="stylesheet" href="stylesheets/babylongrid-default.css">
	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
<body>

<!--Header End -->
<? include('header.php'); ?>
<!--Header End -->
<!--Breadcrumbs start-->
<div class="container">
<div class="row">  

<div class="col-md-12 col-sm-12 box-padding">
<ol class="breadcrumb">
<li><a href="index.php">Home</a></li>
<li class="active"><a href="#">My Account</a></li>
</ol>
       
<div class="col-md-12">
<? include("includes/front.msg.inc.php"); ?>
<h2>My Account</h2> 

        <div class="col-md-1 col-sm-12" style="display:none;"><a href="#">		
		<? if($mem[display_image]!=''){ ?>	
	<img src="<?=SITE_WS_PATH?>web_images/list_thumb/<?=$mem[display_image]?>" />
	<? } else { ?>   
   <img src="<?=SITE_WS_PATH?>images/myaccount.jpg" width="100%" style="border:2px solid #e9e7e7;" />
	<? } ?>
		
		</a></div> 
        <div class="col-md-11 col-sm-12" style="display:none;">
        <div class="lessthen">
		<h2><? if($_SESSION['MEMBER_ID']!=''){?><?=strtoupper($member_data['screen_name'])?><? } ?></h2>  
		<!--<p>Registered on <? //=$member_data['add_date']?>/////
		<? //$date = date("m/d/Y H:i:s", $date);
	//	echo $date;
		?>
		</p>-->  
        </div>  
       	<div class="col-md-12 " style="display:none;"><ul class="list-inline ">    
        <li><span class="glyphicon glyphicon-info-sign"></span> Joined   <span class="wlt_time_key_hours">
		
		<? if(floor($y)>0){ ?><?=floor($y)?> years  <? } ?><? if(floor($mn)>0){ ?><?=floor($mn)?> months <? } ?><? if($day>0){ ?><?=$day?> days <? } ?> <? if($h!=0){ ?> <?=$h?> hours  <? } ?>
		
		</span> 
        <span class="wlt_time_key_minutes"><?=$m?> minutes</span>  ago.</li>
        <li><span class="glyphicon glyphicon-ok-sign"></span> <?=$num_rows?> login's.</li>    
        <li><a href="<?=SITE_WS_PATH?>view-profile.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> View Profile</a></li>
	    </ul></div>
           </div>
           </div>
           </div> 
</div>
                      
         <div class="col-md-12 col-sm-12 box-padding">
		 <div class="col-md-3 col-sm-12">
         <div class="thumbnail">
         <div class="media-object" style="  font-size: 60px;  margin-left: 20px;  margin-top: 10px; color:#ddd; text-align:center">
         <i class="glyphicon glyphicon-user"></i>
         <div class="caption">
         <h3>PROFILE</h3>
          <p>View, modify and update your account details</p>
          <p><a href="profile.php" class="btn btn-default" role="button">View more</a></p>
         </div>
         </div>
         </div>
       	 </div>
            
            
        
  		<div class="col-md-3 col-sm-12">
    	<div class="thumbnail">
        <div class="media-object" style="  font-size: 60px;  margin-left: 20px;  margin-top: 10px; color:#ddd; text-align:center">
        <i class="glyphicon glyphicon-search"></i>
        <div class="caption">
        <h3>View My Ads</h3>
        <p>View Your Existing Ads</p>
		
        <p> <a href="<?=SITE_WS_PATH?>view-myads.php" class="btn btn-default" role="button">View More</a></p>
        </div>
        </div>
  		</div>
		</div>
        
               
       
  		<div class="col-md-3 col-sm-12">
    	<div class="thumbnail">
        <div class="media-object" style="  font-size: 60px;  margin-left: 20px;  margin-top: 10px; color:#ddd; text-align:center">
        <i class="glyphicon glyphicon-pencil"></i>
        <div class="caption">
        <h3>POST NEW AD</h3>
        <p>Create your Ad</p>
        <p><a href="<?=SITE_WS_PATH?>post-ad.php" class="btn btn-default" role="button">View More</a></p>
        </div>
        </div>
  		</div>
		</div>         
            
         
  		<div class="col-md-3 col-sm-12">
    	<div class="thumbnail">
        <div class="media-object" style="  font-size: 60px;  margin-left: 20px;  margin-top: 10px; color:#ddd; text-align:center">
        <i class="glyphicon glyphicon-envelope"></i>
        <div class="caption">
        <h3>MESSAGE</h3>
        <p>View All Message Sent to You</p>
        <p><a href="<?=SITE_WS_PATH?>inbox-message.php" class="btn btn-default" role="button">View More</a></p>
        </div>
        </div>
        </div>
  		</div>
		</div>
          
<div class="col-md-12 col-sm-12"><div class=" border"></div></div>
</div>

</div> 
 
<!-- FOOTER STARTS
========================================================================= -->
<? include('footer.php'); ?>

  <!-- FOOTER END
========================================================================= -->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=SITE_WS_PATH?>js/bootstrap.js"></script> 
</body>
</html>
<? unset($_SESSION['SESS_MSG3']);?>